# Versão do Angular
`CLI: 9.0.6`

# API

Nessa projeto é utilizada a aplicação chamada `Json server` que pode ser instalada diretamente pelo NPM ou Yaarn nos comandos abaixo.

npm install -g json-server
yarn install -g json-server

Lembrando que se estiver usando o linux vai ser preciso colocar o `sudo`, para efeturar essa instalação.

O arquivo onde está salvo os dados da API se encontra no meio dos arquivos inicais do angular, chamado `bd.json`.
Para fazer a aplicação rodar basta está na raiz do projeto e setar o seguinte comando abaixo: 
`json-server --watch db.json` 

Caso precise de mais informações segue o link da documentação da aplicação: `https://www.npmjs.com/package/json-server`.

